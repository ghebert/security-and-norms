 # Summary  
1. [Introduction](#introduction)
2. [References](#references)
3. [Glossary](#glossary)
4. [Principles](#principles)
5. [Digital Trust Service Portal](#digital-trust-service-portal)
6. [Digital Trust Service Management Portal](#digital-trust-service-management-portal)
7. [Data Models](#data-models)
8. [Components View](#components-view)

# Introduction

The goal of this document is to describe how a user can :
* Access a  __Digital Trust Services Portal__ displaying all __Digital Trust Services__ 
* Use these __Digital Trust Services__ 
* Specifie an optional Bridge to simplify integration of any __Digital Trust Services__ 

Accessing these services allow a user to prove its identity in the [Compliance Service](./ComplianceService.md)

This Digital Trust Services Portal will allow a user to sign some documents required by the [Scoring](https://gitlab.com/gaia-x/technical-committee/federation-services/security-and-norms/-/blob/main/Scoring.md).

This document cover specifications of components : 
* A __Digital trust Services Portal__ to display Digital Trusted Services for any user and without any authentication
* An __Management Portal__ to configure the __Digital Trust Service Portal__ 
* A __Bridge__ to simplify integration between Gaia-x and Digital Trust Services

# References 

| Code | Link |
| ------ | ------ |
| W3C Verifiable Credential | https://www.w3.org/TR/vc-data-model/  |
| Gaia-x DID Auth | https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/blob/14-did-sig-proposal-for-a-full-decentralized-authentication-authorization-mode/docs/did-sig.md |
| W3C VP request | https://w3c-ccg.github.io/vp-request-spec/ |
| Gaia-x Architecture| https://gaia-x.eu/sites/default/files/2022-05/Gaia-x%20Architecture%20Document%2022.04%20Release.pdf | 

# Glossary

| Term | Definition |
| ------ | ------ |
| Digital Trusted Service | A trust service provider (TSP) is a person or legal entity providing and preserving digital certificates to create and validate electronic signatures and to authenticate their signatories as well as websites in general. Trust service providers are qualified certificate authorities required in the European Union and in Switzerland in the context of regulated electronic signing procedures. [source Wikipedia](https://en.wikipedia.org/wiki/Trust_service_provider). A Digital Trusted Service MUST be certified by eIDAS |
| Digital trust Services Portal | Service (Web Page) displaying the list of trusted services |
| Participant | A Participant is an entity, as defined in ISO/IEC 24760-1 as “item relevant for the purpose of operation of a domain that has recognizably distinct existence”, which is onboarded and has a Gaia-X Self-Description. A Participant can take on one or more of the following roles: Provider, Consumer, Federator. Section Federation Services demonstrates use cases that illustrate how these roles could be filled. Provider and Consumer present the core roles that are in a business-to-business relationship while the Federator enables their interaction. [source](http://docs.gaia-x.eu/technical-committee/architecture-document/22.04/conceptual_model/#participants) |
|End User| An end User is a human participant |
| Issuer | an Issuer compliant with Gaia-x requirements |
| Bridge | Component that simplifies existing services with the Gaia-x ecosystem |
| Verifiable Presentation (VP) | A set of one or more claims made by an issuer. A verifiable credential is a tamper-evident credential that has authorship that can be cryptographically verified. Verifiable credentials can be used to build verifiable presentations, which can also be cryptographically verified. The claims in a credential can be about different subjects. (Source W3C) | 
| Verifiable Credentials | A standard data model and representation format for cryptographically-verifiable digital credentials as defined by the W3C Verifiable Credentials specification [VC-DATA-MODEL]. |
| DID document | A set of data describing the DID subject, including mechanisms, such as cryptographic public keys, that the DID subject or a DID delegate can use to authenticate itself and prove its association with the DID. A DID document might have one or more different representations. |
| CSP | Cloud Service Provider |
| Candidate | A CSP is considered as a candidate if he is subscribing to the CSP suscribe page but has not completed the subscription process (completion of the form, verification of his declarations) and is therefore not yet registered. |
| User | A CSP is considered as a user if he has completed the CSP subscription process and is therefore registered. |

# Principles

2 process are available. This first one is the integration with a Digital Trust Service integrated with Gaia-x directly :

```mermaid
sequenceDiagram
  autonumber
  actor u as User
  participant portal as Digital trust Services Portal
  participant gxu as Issuer 
  participant ts as Digital Trust Service 
u ->> portal: display Digital Trust Services
u ->> ts: redirect to a Digital Trust Service 
ts ->> u: request a Verifiable Presentation 
u ->> gxu: request a Verifiable Presentation with required fields 
gxu -->> u: return a Verifiable Presentation
u ->> ts: provide Verifiable Presentation from Issuer
note right of ts: User now loggued in Digital Trust Service 
```

| Flow id | Format | Description |
|---|---|---|
| 1 | GET url | return a web page displaying Digital trusted Services |  
| 2 | http redirect | the user get a  VP request as defined by [W3C](https://w3c-ccg.github.io/vp-request-spec/)  |  
| 3 | Verifiable Presentation Request | the service provide a VP request and the user have to get it [see DID Auth](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/blob/14-did-sig-proposal-for-a-full-decentralized-authentication-authorization-mode/docs/did-sig.md) |
| 4 | Verifiable Presentation | the user provides the VP (json file ...) to the Digital Trusted Service as defiend by[DID Auth](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/blob/14-did-sig-proposal-for-a-full-decentralized-authentication-authorization-mode/docs/did-sig.md) | 
| 5 | Verifiable Presentation | the user has to provide the VP request to the issuer and get a Verifiable Presentation as defined by [DID Auth](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/blob/14-did-sig-proposal-for-a-full-decentralized-authentication-authorization-mode/docs/did-sig.md)| 
| 6 | Verifiable Presentation | a valid token if the Verifiable Presentation is valid the user upload the Verifiable Presentation to the service the Verifiable Presentation | 

In order to avoid a complex integration for existing services, a bridge is available to simplify integration in a second process:
```mermaid
sequenceDiagram
  autonumber
  actor u as User
  participant portal as Digital Trust Service Portal
  participant bridge as Bridge
  participant gxu as Issuer 
  participant ts as Digital Trust Service 
u ->> portal: display Digital Trust Services 
u ->> bridge : redirect to the bridge
bridge ->> u: request a Verifiable Presentation
note right of bridge: the bridge is configured <br/>for each Digital Trust Service  to know required fields 
u ->> gxu: request a Verifiable Presentation with required fields 
gxu ->> u: return a VP
u ->> bridge: provide Verifiable Presentation from Issuer
bridge ->> ts:  send datas from the Verifiable Presentation
note right of ts : VP is sent using TLS and <br/>the VP is signed to let Digital Trust Service <br/>MAY perform an other check.
bridge ->> u:  return a redirect to the web browser
u ->> ts: request Digital Trust Service web page and a did in the query to retreive datas sent previously
```

# Digital Trust Service Portal

Any End User (authenticated or not) can display a Web Page providing a list of digital Trusted Services

| Icon | Supplier | Service | Country | Languages | Gaia-x Label | Description |
|---|---|---|---|---|---|---|
| :bust_in_silhouette: | Supplier 1 | eSignature| France | fr,en | :star: :star: :star: | lorem lipsum |
| :cop: | Supplier 2 | eSignature | Germany | de | :star: :star: :star: | lorem lipsum |
| :cop: | Supplier 2 | Digital Wallet| Germany | de, en | :star: :star: | lorem lipsum |
| :guardsman: | Supplier 3 | Digital Wallet| Italy | it, en | :star: | lorem lipsum |

An End User may filter by
* Countries
* Supported languages
* Gaia-x label
* Kind of Service
* Supplier

The table by default is sorted by Gaia-x Level and registration date. 

The Web page is using the administration API to get services list. 

The portal support english, french and german. 

# Digital Trust Service Management Portal 

A Gaia-x User has some management screens and REST API : 
* Management (CRUD) of services list (eSignature, Certificates ...)
* Management (CRUD) of digital trusted services with fields

# Data models 

## Class Diagram 

```mermaid
classDiagram
  class VC
    VC: owner 
  class ServiceOffering 
      ServiceOffering: +String serviceName
      ServiceOffering: +String countryCode
      ServiceOffering: +String logo
      ServiceOffering: +Date registrationDate
  class language
   language: +String iso639_language
  class country
    country: +String iso3166_country
  class kindOfDigitaltrustService 
    kindOfDigitaltrustService: +String name
  class DigitalTrustServices
    DigitalTrustServices: +String urlService
    DigitalTrustServices: +Boolean isBridged
    DigitalTrustServices: +String markdownDescription
  class DigitalTrustServicesBridged
    DigitalTrustServicesBridged: +String jsonRequestVerifiablePresentation
  VC <|-- ServiceOffering
  DigitalTrustServices *-- language
  DigitalTrustServices --> country 
  DigitalTrustServices *-- kindOfDigitaltrustService
  ServiceOffering <|-- DigitalTrustServices
  DigitalTrustServices <|-- DigitalTrustServicesBridged
```

The serviceName is 'Digital Trust Service' and kindOfDigitalService is eSignature, ... to have more details.

A service to manage a list of services : 
| field |  type  | Description |
|---|---|---|
| name | string | eg eSignature, Archive ... | 


# Components View

Components for this solution with their dependencies : 

```mermaid
flowchart TD
    portal("Digital Trust Service Portal") --> adtp("API Digital Trust Service")
    mp("Digital Trust Services Management Portal") --> adtp
    Bridge --> adtp
    adtp --> ac("API Catalog")
    Bridge --> ac
    style portal fill:#87CEFA
    style adtp fill:#87CEFA
    style Bridge fill:#87CEFA
    style mp fill:#87CEFA

```
_in blue, components in scope of this document_

The Portal, The Management Portal and the API Digital Trust Services and the Bridge are specified by this document. <br/>
The API Catalog is provided by an other group, some API are required in order to : 
* create / update / deactivate a Digital trust Service 
* create / update / deactivate some kindOfServices
* search a Digital Trust Service by 
  * Countries
  * Supported languages
  * Gaia-x label
  * Kind of Service
  * Supplier

# APIs provided by API Digital Trust Service

API and Web pages are secure using DID Auth as expected [GXFS-fr Specifications for authentication](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/blob/14-did-sig-proposal-for-a-full-decentralized-authentication-authorization-mode/docs/did-sig.md )

| Verb |  Path  | Rigths | format input | format output |
|---|---|---|---|---|
| GET | /service/{id}| __any user__ without securities | n/a| format service |
| GET | /service | __any user__ without securities | request parameters  | list of services | 
| POST | /service | only Gaia-x Digital Trust Service Administrators | format service | format service |
| PUT | /service | only Gaia-x Digital Trust Service Administrators | format service | format service |
| DELETE | /servicee | only Gaia-x Digital Trust Service Administrators | n/a | n/a |
| GET | /kindOfService | only Gaia-x Digital Trust Service Administrators | x | x |
| POST | /kindOfService | only Gaia-x Digital Trust Service Administrators | x | x |
| PUT | /kindOfService | only Gaia-x Digital Trust Service Administrators | x | x |
| DELETE | /kindOfService | Gaia-x Digital Trust Service Administrators | x | 

## Formats

### Format Service 

This format is defined by [Gaia-x Ontology](https://gaia-x.gitlab.io/technical-committee/service-characteristics/widoco/participant/participant.html) and a specific ontology (gfxsfr). The Ontology gxfsfr extends Gaia-x Service Offering. 

Sample without using bridge : 
```javascript 
{
    "@context": ["http://www.w3.org/ns/shacl#", "http://www.w3.org/2001/XMLSchema#", "http://w3id.org/gaia-x/participant#"],
    "@id": "http://example.org/participant-dp6gtq7i75lmk9p4j2tfg",
    "@type": ["gx-service:serviceOffering"],
    "gax-service:serviceLogo" : {
            "@type": "xsd:string",
            "@value": "http://www.trust.com/logo.png"
        },
        "gax-service:providedBy" : {
            "@id" : "did:web:did.trusted.com"
        },
        "gax-service:termsAndConditions" : {
            "@type": "xsd:string",
            "@value": "http://www.trust.com/termsandconditions.pdf"
        },
        "gax-service:serviceTitle" : {
            "@type": "xsd:string",
            "@value": "Digital Signature Service by Trust.com"
        },
       "dct:description" : {
            "@type": "xsd:string",
            "@value": "a service to sign a pdf with a trusted digital signature"
        },
            "gxfsfr-markdowndescription" : {
            "@type": "xsd:string",
            "@value": "a __service__ to sign a __pdf__ with a trusted digital signature"
        },
       "gxfsfr:ifBridged" : {
            "@type": "xsd:boolean",
            "@value": "false"
        },
       "dcat:keyword" : {
            "@type": "array",
            "@values" : 	["signature","trust"]
        },
       "proof": {
            "type": "JsonWebKey2020",
            "created": "2022-06-17T07:44:28.488Z",
            "proofPurpose": "assertionMethod",
            "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..t_UEs8yG-XXXXXXXXXXX",
            "verificationMethod": "did:web:did.truted.com"
    }
}
```

Sample without using bridge : 
```javascript 
{
    "@context": ["http://www.w3.org/ns/shacl#", "http://www.w3.org/2001/XMLSchema#", "http://w3id.org/gaia-x/participant#"],
    "@id": "http://example.org/participant-dp6gtq7i75lmk9p4j2tfg",
    "@type": ["VerifiableCredential", "LegalPerson"],
    "credentialSubject": {
      "id": "did:web:example.com",
      "gx-participant:registrationNumber": {
        "@type": "xsd:string",
        "@value": "DEANY1234NUMBER"
    },
    "gax-service:serviceLogo" : {
        "@type": "xsd:string",
        "@value": "http://www.trust.com/logo.png"
    },
    "gax-service:providedBy" : {
      "@id" : "did:did.trusted.com"
    },
    "gax-service:termsAndConditions" : {
        "@type": "xsd:string",
        "@value": "http://www.trust.com/termsandconditions.pdf"
    },
    "gax-service:serviceTitle" : {
       "@type": "xsd:string",
        "@value": "Digital Signature Service by Trust.com"
    },
    "dct:description" : {
      "@type": "xsd:string",
      "@value": "a service to sign a pdf with a trusted digital signature"
    },
    "gxfsfr:markdowndescription" : {
      "@type": "xsd:string",
      "@value": "a __service__ to sign a __pdf__ with a trusted digital signature"
    },
    "gxfsfr:ifBridged" : {
      "@type": "xsd:boolean",
      "@value": "true"
    },
    "gxfsfr:sendDatas" : {
      "@type": "xsd:string",
      "@value": "https://api.trust.com/startGaiaxprocess"
    },
    "gxfsfr:urlService" : {
      "@type": "xsd:string",
      "@value": "https://www.trust.com/startProcess"
    },
    "dcat:keyword" : {
      "@type": "array",
      "@values" : 	["signature","trust"]
    },
    "proof": {
      "type": "JsonWebKey2020",
      "created": "2022-06-17T07:44:28.488Z",
      "proofPurpose": "assertionMethod",
      "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..t_UEs8yG-XXXXXXXXXXX",
      "verificationMethod": "did:web:compliance.gaia-x.eu"
    }
  }
}
```

### Request Parameters

In the request url, some parameters allow to filter services: 

| Parameter |  Type  | Description |
|---|---|---|
| country-code | string | country code to 
| lang | string | |
| label | string | |
| kindOfService | string | |
| didSupplier | string | did of the supplier|
| countMax | int65 | return max countMax items |
| page | int64 | return page 1,2 .. |
| sort | string | list of fields to sort the result sort=lang:asc,label:desc |  

This url dts.gaiax.eu/?lang=fr&sort=label:asc will filter to services using french and sorted by label level. 

### List of Services 

```javascript 
{
     "items" : [ 
        { /* format Services */ }, 
        { /* format Services */ }
     ]
}
```

## APIs for Digital Trust Services to integrate the bridge

Digital Trust Services MUST provide some API for the bridge (when used) in order to receive datas 

| Verb |  Path  | Rigths | format input | format output | comments |
|---|---|---|---|---|
| POST | /sendDatas/{id}|  | n/a | format service | {id} is a random uuid v4 and the service provider MUST keep it to store datas | 
| GET | /urlService/{id}|  |  {id} used for the POST| n/a | the user is redirected to the back office with the id used to push datas | 

TODO : est ce que dans le format de service on a bien tous les champs nécessaires ? 

