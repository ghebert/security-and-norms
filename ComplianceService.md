# Table of Contents
1. [References](#references)
2. [Glossary](#glossary)
3. [Introduction](#introduction)
4. [Cloud Service Provider subscribe form](#cloud-service-provider-subscribe-form)
5. [Generation of Verifiable Credentials](#generation-of-verifiable-credentials)
6. [Claims](#claims)
7. [Labelling](#labelling)
8. [Components View](#components-view)

# References

| Code | Link |
| ------ | ------ |
| W3C Verifiable Credential | https://www.w3.org/TR/vc-data-model/  |
| Gaia-x Frameworks and documents | https://docs.gaia-x.eu/framework/ |
| Gaia-x DID Auth | https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/blob/14-did-sig-proposal-for-a-full-decentralized-authentication-authorization-mode/docs/did-sig.md |
| W3C VP request | https://w3c-ccg.github.io/vp-request-spec/ |
| Gaia-x Architecture| https://gaia-x.eu/wp-content/uploads/2022/06/Gaia-x-Architecture-Document-22.04-Release.pdf | 
| Gaia-x Trust Framework | https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/ | 
| Gaia-X Labelling Framework | https://gaia-x.eu/wp-content/uploads/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf |
| Gaia-X Labelling Criteria | https://gaia-x.eu/wp-content/uploads/2022/05/Gaia-X-labelling-criteria-v22.04_Final.pdf |
| Gaia-X Policy Rules Document | https://gaia-x.eu/wp-content/uploads/2022/05/Gaia-X_Policy-Rules_Document_v22.04_Final.pdf |
| Gaia-x Ontology | https://gaia-x.gitlab.io/gaia-x-community/gaia-x-self-descriptions/core/core.html |
| DID | https://gitlab.com/gaia-x/technical-committee/federation-services/self-description-model/-/blob/main/docs/did.md |
| GXFS-FR Ontologies | https://sd-schemas.gaiax.ovh/22.04/sd-attributes.html :triangular_flag_on_post: TODO AVOIR UN LIEN NON OVH |

# Glossary

| Term | Definition |
| ------ | ------ |
| Digital Signature Service | A dignatal service provider is a legal entity providing and preserving digital certificates to create and validate electronic signatures and to authenticate their signatories as well as websites in general. these service providers are qualified certificate authorities required in the European Union and in Switzerland in the context of regulated electronic signing procedures. These services MUST be certified by eIDAS |
| Digital Signature Services Portal | Service (Web Page) displaying the list of trusted services |
| User | Any user like End User or Service Provider |
| Issuer | an Issuer compliant with Gaia-x requirements |
| CSP | Cloud Service Provider |
| Candidate | A CSP is considered as a candidate if he is subscribing to the CSP suscribe page but has not completed the subscription process (completion of the form, verification of his declarations) and is therefore not yet registered. |
| User | A CSP is considered as a user if he has completed the CSP subscription process and is therefore registered. |
| Auditor | A user checking document and with the ability to validate the input. An auditor may be an external company |
| Service Provider | Gaia-x Participant publishing a Service Offering | 
| Consumer | Gaia-x participant using one or more Gaia-x Services |

# Introduction 
__(@Docaposte)__

![image](Assets/ComplianceProcess.png)

A Service Provider must start by providing a Verifiable Presentation for a service to publish it with a Gaia-x Label ([like this sample](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/main/src/tests/provider-instance-test.json))
The Service Provider MUST upload required documents to get a label as defined by the [Gaia-x Labelling framework](https://gaia-x.eu/wp-content/uploads/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf) 
Documents MAY be : 
* a certification/claim as pdf (PDF/a) 
* a certification as Verifiable Credential (from a trusted auditor or issuer) included in the Verifiable Presentation of a Service Offering.
* use a valid eIDAS Certificate to sign Verifiable Credential with a valid DID

The Compliance Service perform technical checks as defined in the [trust framework](http://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#trust-anchors)
* syntactic correctness. 
* schema validity.
* cryptographic signature validation (for all signatures and a full check : a valid certificate, valid root ...)
* attribute value consistency.
* attribute value verification.

The compliance service provide ( [source Trust Anchors|(http://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#trust-anchors) ):
* as a Verifiable Credential: If the compliance is validated, the Gaia-X Compliance Service issues a Verifiable Credential that can later be inserted into the Self-Description. This method aligns with the self sovereign principle of the holder being in charge of the information.
* If the Gaia-X Compliance Service is called via the Gaia-X Registry, the Gaia-X Registry will emit an event to synchronize Catalogues. The event contains the URL the of the Self-Description file. The Gaia-X Registry is defined in the next section.

Some links to Participants to some [Digital Signature Services](./DigitalSignatureServices.md) to prove Participant's identity : 
  * services to get a valid certificate
  * sign a document 

The compliance MAY store the Verifiable Presentation and MUST store Verifiable Claims and revokations associated.

# Cloud Service Provider subscribe form 
__@Atos__

## Portal & API to create a service 

User connects to the portal and a form is displayed.  

* User accessing the enrollment service can be previously authenticated thanks to an Identity Provider (link to lot 1.1)
* This will be materialized by the provisioning of a JWT (ref RFC7519) token containing claims 
* The token is provided in the Authorization header of the HTTP request with format “bearer<space><token>”
* confidentiality and authenticity are offered by HTTPS (mandatory - TLS1.3 recommended)
* The JWT token can be provided by 
  * An IAM known as Gaia-X trust provider : here we can think about the use case where a company already providing a Gaia-X service wants to enroll a new one
  * An IAM considered trusted by the enrollment service itself, it can be a national IDP like France connect or any other listed in the service context with access to public key to check the validity of the token.
  * Any IAM; the claims contained in the JWT token can be proposed as pre-filled values in the registration form The registration form itself is composed of an HTML form listing all inputs needed for the enrollment and a REST api service used by the form and who can be used by a program

The input expected can be in the form of 
* Text 
  * Name of the company
  *	Name of the service 
  * Description of the service
* Files 
* certificate (like ISO)
* Links to online certificates
  * X509 Certificate
* Verifiable Credentials (or link to a VC) - Optionnaly (out of scope November ?) > a QR code is diplayed representing a request presentation : Verifier (portal) requests to Holder a presentation which describes values that need to be revealed and predicates that need to be fulfilled.
* Verifiable Presentation directly (complex to do, the user cannot know what the portal needs as information)
* Content definition (technical and documentary) of the subscribe form and mockup (certifications list etc. from the PRD)  ( + API for a CSP subscription ?)
Claims : a service can depend on another (label inheritance)
* Requests/submission of supporting documents : Description and mockup (+ upload of VC links)
* The service shall provide interfaces to cancel the current subscription (no action at all afterward) or to delete the subscription. 
* The update of the subscription is possible (pending state) to allow the candidate to draft its form
  * (A valider) the verification services (§2.2) could be called dynamically and the form can display immediately a validation status if available.
* Once the form submitted, the verification process can occur, the candidate can display the form and a component will inform him regarding the status
  * In following use cases, validation can fail 
    * Expiration or revocation of an x509 certificate
    *	... (see details in §5)

![image](Assets/portal.png)

## Claims

Clamais MUST be integrated into a Verifiable Pressentation to send to the Compliance Service.

In particular, there are two kinds of proof that can be provided :
* VC signed with a private key linked to a X.509 certificate
  * the certificate has to be recognized as trusted (eIDAS)
  * VC claims must correspond to certificate's extensions (especially Subject DN)
    * country : C= in Subject DN
    * organization : O=
    * registrationNumber : organizationIdentifier (OID.2.5.4.97)
    * city : L= 
  * the verification service verifies the values of certificate against VC claims

> TODO : a revoir car le principe sera plutot de signer la VP avec un certificat eidas avec un DID Document avec la clef publique RSA. 
> normalement ca suffit.

* VC containing a signed document (PAdES) or a link to the document
  * VC is self signed with any key, this information is ignored
  * VC includes the document in base 64 or a link to the document and a hash corresponding to the document, in this last case, the hash must be verified.
  * Document signature verification must be made as a first start of verification, including certificate validity.
  * VC claims must correspond to certificate's extensions (especially Subject DN) [same verifications as previous §]
    * country : C= in Subject DN
    * organization : O=
    * registrationNumber : organizationIdentifier (OID.2.5.4.97)
    * city : L= 
  * information can be updated trustfuly with data included in the document, that are considered as self declaration 
  * this format can be extended to other formats that are machine readable (XAdES, JAdES).
  
## Supporting documents required 
__@IMT (@Outscale @Atos @Docaposte @OVH)__

* Required documents table (linked with the PRD document) by certification
* Document upload, Link upload (VC) (+API)

COMPLETE LE July 6th

For the Label, the list of proof by Criterion

* Contractual governance
    * C1, C2, C3, C4 <- self-assessment through the trust framework
* Transparency
  * C5, C7, C8, C9, C10, C11, C12, C13 <- self-assessment through the trust framework
  * C6 <- self-assessment through the trust framework An accepted standard is ISO19944
  * C14 <- Gaia-X trust framework checking the self-description (existence of a Policy in SD)
  * C15 <- Gaia-X trust framework checking the self-description (existence of a verified Identity in SD)
  * C16 <- Gaia-X trust framework checking the self-description (conformant SD)
  * C17 <- Gaia-X trust framework checking the self-description (consistent attribute)
  * C18 <- Gaia-X trust framework checking the self-description (consumer has identity provided by te federator) ??
* Data Protection
  * C19, C20, C21, C22, C23, C24, C25, C26, C27, C28, C31  <- L1 self verified, L2/L3 (accepted standard codes of conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc.Art. 42 GDPR.)
* Security
  * C32 -> C51  <- Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA
* Portability
* C52, C53 -> Accepted Standards: SWIPO IaaS, SaaS and merged code CoC
* C54, C55 -> Gaia-X self-declaration through the trust framewor
* C56-> C61 -> Gaia-X self-declaration

Claims required by these rules:
* [X.509 Claim](##X509-Claim)
* [pdf signed Claim](##Pdf-Claim)
* TO COMPLETE

## Supporting document validation

Supported organizations list (ability to VC emission?) 

### Supporting document validation in the current context

#### Automated exchange with official certification lists 
__@Outscale__

Servies to provide:
* eiDAS	Issuers of Qualified Certificate for Electronic Signature as defined in eIDAS Regulation (EU) No 910/2014
  (homepage: https://esignature.ec.europa.eu/efda/tl-browser/#/screen/home)
  (machine: https://ec.europa.eu/tools/lotl/eu-lotl.xml)
> find an european service or create an api to store CA root 
* Gaia-X	To be defined after 2022Q1.
> design a DID registry (did:web, ...)
* Moke up for November:
    * a service to retreive a scoped certificates list by an [LEI Code](https://schema.org/leiCode) as described by [GLEIF](https://www.gleif.org/en/about-lei/get-an-lei-find-lei-issuing-organizations)

Out of scope for November:
* Documentary analysis for extracting relevant information, example : analysis of AFNOR certificate to extract the company, date, type of certificate
@Bastien  : How they control their clients certification
* Certification validation systems list table
* Exchange format with certification validation systems
* Exchanged flows ( Inputs/Outputs, etc.)
* gleif	List of registered LEI issuers.
(homepage: https://www.gleif.org/en/about-lei/get-an-lei-find-lei-issuing-organizations)
(machine: https://api.gleif.org/api/v1/registration-authorities)
* State	The Trust Service Providers (TSP) must be a state validated identity issuer.
  - For participant, if the legalAddress.country is in EEA, the TSP must be eiDAS compliant.
  - Until the end of 2022-Q1, to ease the onboarding and adoption this framework, DV SSL can also be used.
  - Gaia-X Association is also a valid TSP for Gaia-X Association members.
* EDPB CoC	List of Monitoring Bodies accreditated to the Code of Conduct approved by the EDBP
(list of EDBP’s CoC: https://edpb.europa.eu/our-work-tools/documents/our-documents_fr?f%5B0%5D=all_publication_type%3A61&f%5B1%5D=all_topics%3A125)

#### Link to automated supporting document analysis services

Manual Document Analysis is __Out of scope__ for november 2022. 

In some future release above November 2022:
* Supporting document analysis service directory by document type
* Content description and mockup of the service directory page by document type 
* Supporting document  analysis service description and subscribe standard pages (+ link to the API catalog by service)

### Digital Signature Service
__@Atos, @Docaposte__

The goal is to provide a portal to participants allowing them to choose a Digital Signature Service. 
This Digital Signature Service allows:
* to get eIDAS certificates
* to sign documents 

Certificates are required to sign a Verifiable Presentation to register any Service Offering. The certificate MAY be any certificate but to get some labels eIDAS certificates (or a document signed by a eIDAS certificate). If a participant have not yet an eIDAS certificate, the portall allows it to find a provider.
The other option for a participant is to use an eSignature service to sign a document and add it in the Verifiable Presentation.

```mermaid
flowchart TB
  id1((start))
  id2{certificate from <br/>an authority recognized by EU ?}
  id1-->id2
  id3[[Sign a VP with it]]
  id2-- yes ---id3
  id4[[Use a Digital Signature Service ]]
  id2-- no ---id4
  id5{did you choose a certificate ?}
  id5-- yes ---id3
  id5-- no ---id6
  id6[[signature process]]
  id7[[insert claims in VP <br/>and VP unsigned]]
  id3-->id7
  id4-->id5
  id6-->id7
  id8[[check certificate used by VP]]
  id9[[insert  certification claims into VP]]
  id8-->id9
  id7-->id9
  id3-->id8
  id10[[check Verifiable Presentation or claims]]
  id9-->id10
  id11[[generate Verifiable Credential signed]]
  id10-->id11
  id12[(Service Verifiable Credential)]
  id11-->id12
```

* If the candidate to the service creation has credentials validated by an authority recognized at EU level (https://esignature.ec.europa.eu/efda/tl-browser/#/screen/home) , he will sign the Verifiable Presentation with its x509 certificate and provide a did:web for that certificate. 
* self declared information should be digitally signed by a Digital Signature Service Prodiver. 

More informations about Digital Signature Service providers are available in a [specific document](https://gitlab.com/gaia-x/technical-committee/federation-services/security-and-norms/-/blob/main/DigitalTrustServices.md) 

#### Manual verification of a document (claim)

Manual verification if __out of scope__ for November.

#### Receipt of Verifiable Credentials from organizations 
__@Outscale__

Participants provides Verifiable Credentials including from organization. 
These organizations MUST be in a list from the Trust anchor component. 

These checks are __out of scope__ for november 2022.

# Labelling 

## Generation of a GAIA-X label based on data from the subscribe form
__@IMT (@Outscale @Atos @Docaposte @OVH )__

* Labeling algorithm based on the Policy Rules Documents. How a CSP could inherit GAIA-X label from another ?
* Algorithm maintenance : use a ruleset old version ?

# Generation of Verifiable Credentials

## Tool to build Verifiable Credentials (Work Package 1.5.4.1)

### Goal 
The purpose of the work package is to deliver Verifiable Credentials that will assert the compliance to standards required by Gaia-x. 
Those verifiable credentials (VCs) will be issued in accordance with the compliance scheme of the compliance references.

In a single input file, for any compliance reference, any service and any location (able to provide required informations, would be provided to generate the VCs.

### Input Data
This input file identify SHALL identify:
* for the compliance reference
  * an identifier of the compliance reference
    * description of the compliance reference
    * version of the compliance reference
    * validity date of the compliance reference
    * link  targeting the reference file with the compliance reference in a pdf format
    * available third party for compliance assertion
  * an identifier of the compliance reference owner
  * description of the compliance reference owner
* for the assertion method
  * Self assertion method
  * Third-Party assertion method
  * Third Party Identifier
  * for the service:
    * an identifier of the service
    * a description of the service
    * location where the service is available
    * for the location;
      * an identifier of the location
      * a description of the location 

### Deliverables

For each compliance reference, each service and each location as described in the input data section, VC shall be delivered:
* in JSON-LD format
* compliant with W3C Verifiable Credentials Data Model v1.1 recommendation 
* whose "subject" will be the service identified by a unique identifier in reference to the DID Web.
* whose compliance reference will be identified by a unique identifier with reference to the DID of the authority that issued this compliance reference 
* whose conformity certification will be identified by a unique identifier with reference to the DID of the certification authority, if applicable (in the case of a "self-assessment" with reference either to the DID of the authority in charge of the reference system or the participant DID » 
    * signed by the participant with reference to the participant DID 
    * including, if necessary, location information with reference to participant DID 
The VCs will MUST be available:
* to the public by participants as files in JSON-LD format 
* using the did:web format for managing its DID

## Repositorty for a participant (Work Package 1.5.4.2)

### Goal
__This part is out of scope for November 2022__

The goal of this batch is to provide a repository for a participant. 

We have retained that the following roles as partners engaged in the compliance framework be handled in the current work package.
* Cloud Service Provider
* Organization managing compliance standards
* Certification body for Third-Party compliance assertion method,
* GAIA-X as manager of a “Trust Framework” 
 
### Input Data
A complete ontology for each of the objects handled by the 4 roles would be provided as input data. It assumes that the Organizations managing compliance standards and the certification bodies will keep a global certification ontology with reference to a document.
 
The description of services (self-description), locations (geographical area) and labels will be likely to be composed. 
 
__The ontologies necessary for the provision of the VC system for the implementation of the VC system are provided by Gaia-X.__ 
 
## Deliverables
For each here above defined roles the system will implement of the following functions:
* registration of a did for the actor in question (verification by RSA 4096 key)
* implementation of a WEB server to access the VCs and the DID
* import of VCs input data in batch mode for generation of CVs (input file in CSV format) 
* selection of VCs to produce and checks necessary for the production of the VCs (only the VCs invoked in the batch file will be checked) 
* production and availability on the VC website for interaction with other actors 

The application delivered will be identical for the 4 roles. The adaptation of the application to the role will be carried out through the initial configuration of the latter. Once deployed the application will not likely change the role it performs. 
The realization of the complete VC system requires the implementation of 4 roles. 

# Service lifecycle management

All Verifiable Credentials MUST have a validity period.
All Verifiable Credentials/Presentations MAY be reoked by the owner and the owner only. 

All Verifiable Credentials MUST manage a [revokation list as defined by w3c](https://w3c-ccg.github.io/vc-status-rl-2020/#:~:text=The%20verifiable%20credential%20that%20contains,that%20includes%20the%20RevocationList2020Credential%20value.&text=The%20type%20of%20the%20credential,revocation%20list%2C%20MUST%20be%20RevocationList2020%20.) 

It's out of scope for November 2022 but it's a nice to have for November 2022. 

# Components View 

Principles for components: 
* MUST provide an API
    * API MUST be a standard each time it's possible 
    * API MUST be a REST API. An other way to provide API MUST be justified.
* MUST provide a way to install and start it
    * Components may be provided as containers 
* MAY be implemented in any language. 
* MAY reuse existing components each time it's possible 

## Schema 

The compliance service is integrated in this eco system:
```mermaid
flowchart TD
    DistributedAuthentication --> WebCatalog
    WebCatalog --> DistributedAuthentication
    form('Cloud Service Provider subscribe form') --> APICompliance
    Notorization --> form
    WebCatalog --> WebCompliance
    WebCompliance --> APICompliance
    APICompliance --> pr("Policy Rules")
    WebCompliance --> adtp("Digital Signature Service")
    APICompliance --> APICatalog
    adtp --> APICatalog
    portal("Portal") --> DistributedAuthentication
    CheckCertificate --> APICompliance
    APICompliance --> CheckCertificate 
    CheckCertificate --> TrustAnchorRegistry 
    FederationCatalog --> WebCompliance
    vci("VC Issuer") --> APICompliance
    style WebCompliance fill:#87CEFA
    style APICompliance fill:#87CEFA
    style CheckCertificate fill:#87CEFA
    style CheckCertificate fill:#87CEFA
    style vci fill:#87CEFA
    style form fill:#87CEFA
    style TrustAnchorRegistry fill:#87CEFA
```
_in blue, components in scope of this document_

The output is synchronous when possible (all check automated)

| Component | Description | Batch | Owner | 
|---|---|---|---|
|DistributedAuthentication| Protocols / API / Services to use to authenticate users and a profile. To clarify ASAP | 1.1 | | 
|WebCatalog| Web pages to allow a user to create/update a Digital Trust Service | 1.3 |  |
|WebCompliance| Web pages to allow a user to add documents (ISO 27001 certification ...) to get a label | 1.5 | |
|APICompliance| API to add documents required to label a service| 1.5 | | 
|APICatalog| API to create/update services or search services | 1.3 | | 
|TrustAnchorRegistry| Registry of valid trust anchors | ?  | | 
|Policy Rules| Registry of policy rules | ?  | | 

| Portal| Gaia-x Portal of federation Portal | ? | |
| VC Issuer | Tool to generate Verifiable Presentations for participants. | 1.5 | |  

The API Compliance Service is organised in 3 components: 
```mermaid
flowchart TD
  Participant --> Orchestration 
  Orchestration --> Verifies
  Orchestration --> lba("Label Rule Assignement")
  Orchestration --> vcg("Generation of the VC signed with a label")
  Orchestration --> store('Store the VC')
  Orchestration --> ic('integration to the catalog')
```
| Component | Description | Batch | Owner | 
|---|---|---|---|
| Verifies |  Perfrorms all technical checks that may be done like checking certificate validity, compliance with format and ontologies defined by Gaia-x | 1.5 | | 
| Label Rule Assignement | Get the VP and apply rules from the Policy Rules component to attribute a label and call then request the generation of the Verifiable Credential  | 1.5 | | 
| Generation of the credential | This component generate the verifiable credential, sign it and perform some integration like storing the VC and if allow integrate it in the Federation Catalog  | 1.5 | | 
| Orchestration | The component Orchestration have to manage the orchestration of participants request. Calls MAY be asynchronous. The Orchestration MAY store required information to manage request states.  | 1.5 | | 

The API Compliance MUST provide a REST API to sign a Verifiable Credential with in input ([source](https://gitlab.com/gaia-x/lab/compliance/gx-compliance)): 
```javascript 
{
    "@context": "https://www.w3.org/2018/credentials/v1",
    "type": "VerifiablePresentation",
    "holder": "did:web:www.cloudprovider.eu/vp_service_50",
    "verifiableCredential": [
      {
        "@context": ["http://www.w3.org/ns/shacl#", "http://www.w3.org/2001/XMLSchema#", "http://w3id.org/gaia-x/participant#"],
        "@id": "http://example.org/participant-dp6gtq7i75lmk9p4j2tfg",
        "@type": ["VerifiableCredential", "LegalPerson"],
        "credentialSubject": {
          "id": "did:web:did.cloudprovider.com",
          "gx-participant:registrationNumber": {
            "@type": "xsd:string",
            "@value": "DEANY1234NUMBER"
          },
          "gx-participant:headquarterAddress": {
            "@type": "gx-participant:Address",
            "gx-participant:country": {
              "@type": "xsd:string",
              "@value": "DEU"
            }
          },
          "gx-participant:legalAddress": {
            "@type": "gx-participant:Address",
            "gx-participant:country": {
              "@type": "xsd:string",
              "@value": "DEU"
            }
          }
        },
        "proof": {
          "type": "JsonWebKey2020",
          "created": "2022-06-17T07:44:28.488Z",
          "proofPurpose": "assertionMethod",
          "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..t_UEs8yG-XXXXXXXXXXX",
          "verificationMethod": "did:web:vc.transmute.world#z6MksHh7qHWvybLg5QTPPdG2DgEjjduBDArV9EF9mRiRzMBN"
        }
      },
      { 
        "@context": ["http://www.w3.org/ns/shacl#", "http://www.w3.org/2001/XMLSchema#", "http://w3id.org/gaia-x/participant#"],
        "@id": "http://example.org/participant-dp6gtq7i75lmk9p4j2tfg",
        "@type": ["VerifiableCredential", "CertificateeIDAS"],
        "credentialSubject": {
          "owner" : "did:web:did.cloudprovider.eu" , 
          "publicKey" : "GDDSGGGD4566436"
        },
          "proof": {
             "type": "x509",
             "created": "2021-11-13T18:19:39Z",
             "verificationMethod": "https://example.edu/issuers/14#key-1",
             "proofPurpose": "assertionMethod",
              "proofValue": "z58DAdFfa9SkqZMVPxAQpic7ndSayn1PzZs6ZjWp1CktyGesjuTSwRdoWhAfGFCF5bppETSTojQCrfFPP2oumHKtz"
          }
      }
 ],
    "proof": {
      "type": "RsaSignature2018",
      "created": "2020-03-27T21:04:27Z",
      "verificationMethod": "did:web:vc.transmute.world#z6MksHh7qHWvybLg5QTPPdG2DgEjjduBDArV9EF9mRiRzMBN",
      "proofPurpose": "assertionMethod",
      "jws": "eyJhbGciOiJFZERTQSIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19.jYAsUftMzjTw6Dt9DZnM5wbiPzLyGMrd4xYZ1f74bmoInDGP9QufL7gGCTJdIiPCtj26rX_H6Sa-gAcRfdQOBw"
    }
  }
```

The output of the Gaia-X Compliance service is a VerifiableCredential containing the id and hash of the compliant VerifiableCredential from the input (source https://gitlab.com/gaia-x/technical-committee/federation-services/self-description-model/-/blob/main/docs/compliance.md)

```javascript
{
  "id": "http://example.edu/complianceResult/1",
  "type": [
    "VerifiableCredential"
  ],
  "issuer": "https://example.edu/issuers/565050",
  "issuanceDate": "2010-01-01T00:00:00Z",
  "credentialSubject": [
    {
      "type": "gaiaxCompliance",
      "id": "http://example.edu/verifiableCred/1",
      "hash": "0f5ced733003d11798006639a5200db78206e43c85aa0386d7909c3e6c8ed535"
    },
    {
      "type": "gaiaxCompliance",
      "id": "http://example.edu/verifiableCred/2",
      "hash": {
        "@value": "0f5ced733003d11798006639a5200db78206e43c85aa123456789789123456798",
        "@type": "xsd:string",
        "@checksumtype": "SHA-256"
      }
    }
  ],
  "proof": {
    "type": "JsonWebSignature2020",
    "created": "2022-06-12T19:38:26.853Z",
    "proofPurpose": "assertionMethod",
    "jws": "z2iiwEyyGcqwLPMQDXnjEdQU4zGzWs6cgjrmXAM4LRcfXni1PpZ44EBuU6o2EnkXr4uNMVJcMbaYTLBg3WYLbev3S",
    "verificationMethod": "did:web:compliance.gaia-x.eu"
  }
}
```

# API

## REST API

The compliante service must provide a REST API 

| Verb |  Path  | Rigths | format input | format output | Description |
|---|---|---|---|---|---|
| POST | /compliance/| any gaia-x participant | Verifiable Presentation Service | Verifiacle Credential  |

## Format Verifiable Presentation Service 

This Verifiable Presentation is a Service Offering as defined in Gaia-x Ontology. 
This verifiable Presentation MUST include some claims to get a label like X509 or pdf. 

## Format Verifiable Credential 

The compliance service MUST return a verifiable credential when automated compliance tests may be done: 
```javascript 
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://www.w3.org/2018/credentials/examples/v1"
  ],
  "id": "http://example.edu/credentials/1872",
  "type": ["VerifiableCredential", "x509"],
  "issuer": "https://example.edu/issuers/565049",
  "issuanceDate": "2010-01-01T19:23:24Z",
  "credentialSubject": {
    "id": "did:example:ebfeb1f712ebc6f1c276e12ec21",
    "service": "did:example:ebfeb1f712ebc6f1c276e12ec21",
    "label": "1"
  },
  "proof": {
    "type": "RsaSignature2018",
    "created": "2017-06-18T21:19:10Z",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "https://example.edu/issuers/565049#key-1",
    "jws": "eyJhbGciOiJSUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..TCYt5X"
  }
}
```

## X509 Claim 

Certificates MUST be used to sign the Verifiable Presentation with a did:web providing information about keys like this one : 

```javascript 
{
  "@context": ["https://www.w3.org/ns/did/v1", "https://w3id.org/security/suites/jws-2020/v1"],
  "id": "did:web:cloudprovider.eu",
  "verificationMethod": [
    {
      "@context": "https://w3c-ccg.github.io/lds-jws2020/contexts/v1/",
      "id": "did:web:cloudprovider.eu#1",
      "publicKeyJwk": {
          "kty": "RSA",
          "n": "oiV5WxxfuZGVm02szPbvEaiWMr9EwLrwXytoM65PaVdiCqxe9xSsgvtdqeCjyaxNub2TECDHOiMdhg0gs7ww8KGrnw54g8gyIO48UIh5t0IQTmzS9URdW5wU7-UbZOIErDGiy7rcgQ_tZcop1y57cOEgDpHniNJcX97nEpGGZu0tLl57hsqtxrMuWlR7xr5E7Ni9Mf2Dm_Ya9l2kUFygYaQPwy_Fmo5quO3P9Hvf7pk0qMC0hv2KtZ4ObkIIDfKDxdKVDJ-9zO8NYtl861-lH9CD_JFbJc3rqj8gBUlXa-XDgZXOibqg31YpFGK4pokDCFIx8xFYUpkwkLXDq9qtJ_FtHRNeR7zQ71nFGpocaeCp5qq3jVjNrx9kjcPJFDRGbKEuKNbvebGhOjnWwkTeDlgFlsZZalUQuhTea5crDjX-8vEGxMt3kg9BvcQZHWqIuoPfDRCvpvRfuI62G8Sw9QI2TD9yKOxv4qPpa0cnPo6d_Neldul9xAk2-7Eq7ehiT8ok5hG7ExB5aTCeaCk9X9vTBX09caw4ELdoPV5joLdPC1zJP_wS1JCZEnezpZ4sE3vzZ8LqJuGUb6q7US8kMgc3xxVIOoCwTvNkOnG5fFCL7bY23vCf29hFLSnREq0hpb4UZg7irC0YafH8hrs4idS3hYL4Z1-dIev3t0hHOEc",
          "e": "AQAB",
          "alg": "PS256",
          "x5u": "https://did:web:cloudprovider.eu/.well-known/0002_chain.pem"
      }
    }
  ],
  "assertionMethod": [
    "did:web:cloudprovider.eu#1"
  ]
}
```

This did:web MUST include a x5u attribute to get the full chain.
This did allow the compliance service to check some attributes in the Verifiable Presentation. 
The compliance service MUST check the certification chain and check if it's an eIDAS certificate or not with this chain. 
Fields like x5u are defined in this [w3c specification for JWT](https://datatracker.ietf.org/doc/html/rfc7515)

## Pdf Claim 

A pdf claim is a Verifiable Credential.

link to a pdf signed : 
```javascript 
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://www.w3.org/2018/credentials/examples/v1"
  ],
  "id": "http://example.edu/credentials/1872",
  "type": ["VerifiableCredential", "x509"],
  "issuer": "https://example.edu/issuers/565049",
  "issuanceDate": "2010-01-01T19:23:24Z",
  "credentialSubject": {
    "id": "did:example:ebfeb1f712ebc6f1c276e12ec21",
    "pdf": {
      "link": "did:web:www.documentsigned.fr/id/doc2333.pdf",
      "hash": "fffef45434565Ffjeifjifjie",
    },
  },
  "proof": {
    "type": "RsaSignature2018",
    "created": "2017-06-18T21:19:10Z",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "https://example.edu/issuers/565049#key-1",
    "jws": "eyJhbGciOiJSUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..TCYt5X"
  }
}
``` 

pdf signed included in base64 in a text field :
```javascript 
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://www.w3.org/2018/credentials/examples/v1"
  ],
  "id": "http://example.edu/credentials/1872",
  "type": ["VerifiableCredential", "x509"],
  "issuer": "https://example.edu/issuers/565049",
  "issuanceDate": "2010-01-01T19:23:24Z",
  "credentialSubject": {
    "id": "did:example:ebfeb1f712ebc6f1c276e12ec21",
    "pdf": {
      "name": "file.pdf ",
      "content": "GTGEFEF654FGFFCGGYVDDWXBNUYREZ35643",
    },
  },
  "proof": {
    "type": "RsaSignature2018",
    "created": "2017-06-18T21:19:10Z",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "https://example.edu/issuers/565049#key-1",
    "jws": "eyJhbGciOiJSUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..TCYt5X"
  }
}
```

## Main dependencies

Distributed Authentication MUST provide an openid connector. 

APICatalog MUST provide:
* an API to search a service with crietrias (not ready :red_circle:)

API Compliance MUST provide:
* an API to start a new compliance (not ready :red_circle:)
* an API to upload and store VC and pdf signed (not ready :red_circle:)
* an API to check a signature or an uploaded document (not ready :red_circle:)
* an api to validate / abord a compliance (not ready :red_circle:)

Digital Signature Service MUST provide: 
* an entry point to start a compliance (not ready :red_circle:)

TrustAnchorRegistry MUST provide :
* an API to konw if a certificate is in the validation list (or not) and for a given role (eg ANSII is allow to sign a SecNumCloud certification but not an ISO 27001 certification).  (not ready :red_circle:)
